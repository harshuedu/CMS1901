Assignment 1
Practical Computing (Part 2)
In this assignment, you will make a git project on your machine and then host that on gitlab. Follow the
instructions given below:
1. Make a project directory on your machine with name same as your roll number: \CMS19**". Please
make sure that CMS appears in Capitals, and there are no spaces in the directory name.
2. Now add 3 files to the project and make some changes and commit them. There should be at least
2 commits. For example, you can make the first commit after adding all the files, and then you can
make some changes and commit again.
3. Create an account on gitlab.com and create a remote repository there. Make sure that this repository
is public so that I should be able to clone it.
4. Add that repository as your remote for the current project, and push the changes to the remote. Make
sure that changes are reflected on the gitlab interface.
5. Finally, upload one of the files in your project on Moodle as a submission for the assignment. This is
just to keep track of submissions.